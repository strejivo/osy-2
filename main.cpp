#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cstdint>
#include <cassert>
#include <unistd.h>
using namespace std;

#define SECTOR_SIZE 512
#define MAX_RAID_DEVICES 16
#define MAX_DEVICE_SECTORS (1024 * 1024 * 2)
#define MIN_DEVICE_SECTORS (1 * 1024 * 2)

#define RAID_OK 0
#define RAID_DEGRADED 1
#define RAID_FAILED 2
#define RAID_STOPPED 3

struct TBlkDev {
    int m_Devices;
    int m_Sectors;
    int (*m_Read) (int, int, void *, int);
    int (*m_Write) (int, int, const void *, int);
};
#endif /* __PROGTEST__ */

int createIntFromBoolArray(bool * arr, int size){
    int ret = 0;
    for (int i = 0; i < size; i++){
        ret<<=1;
        if (arr[i]) ret += 1;
    }
    return ret;
}

void fillArrayFromInt(bool * arr, int size, int val){
    for(int i = 0; i < size; i++) arr[i] = (val>>i)&1;
}

void XORArrays(unsigned char out[SECTOR_SIZE], const unsigned char a [SECTOR_SIZE]){
    for (int i = 0; i < SECTOR_SIZE; i++) out[i] = out[i]^a[i];
}

class RAID{
public:
    RAID(TBlkDev * dev){
        wrkDev = *dev;
        areOK = new bool [dev->m_Devices];
        bool * tmpNeedsVote = new bool [dev->m_Devices];
        int * votedRatio = new int [dev->m_Devices];
        for(int i = 0; i < dev->m_Devices; i++){
            tmpNeedsVote[i] = true;
            votedRatio[i] = 0;
        }
        unsigned char tmp [SECTOR_SIZE];
        for(int i = 0; i < dev->m_Devices; i++){
            if (dev->m_Read(i, 0, tmp, 1) != 1) {
                areOK[i] = false;
                tmpNeedsVote[i] = false;
                continue;
            }
            for(int j = 0; j < dev->m_Devices; j++){
                if((tmp[1]>>j)&1) votedRatio[dev->m_Devices-j-1] +=1;
                else votedRatio[dev->m_Devices-j-1] -=1;
            }
        }
        for(int i = 0; i < dev->m_Devices; i++){
            if(tmpNeedsVote[i]){
                if(votedRatio[i] > 0) areOK[i] = true;
                else areOK[i] = false;
            }
        }
        delete [] tmpNeedsVote;
        delete [] votedRatio;
    }
    ~RAID(){
        unsigned char tmp[SECTOR_SIZE];
        for (int i = 0; i < SECTOR_SIZE; i++) tmp[i] = 0;
        prep:
        tmp[1] = createIntFromBoolArray(areOK, wrkDev.m_Devices);
        for (int i = 0; i < wrkDev.m_Devices; i++){
            if(areOK[i]){
                tmp[0] = i;
                if (wrkDev.m_Write(i, 0, tmp, 1) != 1){
                    areOK[i] = false;
                    goto prep;
                }
            }
        }
        delete [] areOK;
    }
    bool getData(int dev, int line, unsigned char a[SECTOR_SIZE]){
        if(getState() == RAID_OK){
            if(wrkDev.m_Read(dev, line+1, a, 1) != 1){
                areOK[dev] = false;
            } else return true;
        }
        if(getState() == RAID_DEGRADED){
            if(areOK[dev]){
                if(wrkDev.m_Read(dev, line+1, a, 1) != 1){
                    areOK[dev] = false;
                    return false;
                } else return true;
            }
            for (int i = 0; i < SECTOR_SIZE; i++) a[i] = 0;
            unsigned char tmp [SECTOR_SIZE];
            for(int i = 0; i < wrkDev.m_Devices; i++){
                if (i == dev) continue;
                if(wrkDev.m_Read(i, line+1, tmp, 1 ) != 1 ) {
                    areOK[i] = false;
                    return false;
                }
                XORArrays(a, tmp);
            }
            return true;
        }
        else if (getState() == RAID_FAILED) return false;
        return true;
    }
    int getState(){
        bool alreadyOneIsFailed = false;
        for (int i = 0; i < wrkDev.m_Devices; i++) {
            if (!areOK[i]) {
                if (alreadyOneIsFailed) return RAID_FAILED;
                alreadyOneIsFailed = true;
            }
        }
        if (alreadyOneIsFailed) return RAID_DEGRADED;
        return RAID_OK;
    }
    bool getParityData(int line, unsigned char ret[SECTOR_SIZE]){
        return getData(getParityID(line), line, ret);
    }
    int getParityID(int line){
        return ( line + wrkDev.m_Devices - 1 ) % wrkDev.m_Devices;
    }
    TBlkDev wrkDev;
    bool * areOK;
};

RAID * curr = NULL;

// <editor-fold defaultstate="collapsed" desc="">

int RaidCreate(TBlkDev * dev) {
    if (dev == NULL) return 0;
    else if (dev->m_Devices > MAX_RAID_DEVICES || dev->m_Devices < 3) return 0;
    bool * areOK = new bool[dev->m_Devices];
    for (int i = 0; i < dev->m_Devices; i++) areOK[i] = true;
    unsigned char tmp [SECTOR_SIZE];
    for (size_t i = 0; i < SECTOR_SIZE; i++) tmp[i] = 0;
prepCreate:
    tmp[1] = createIntFromBoolArray(areOK, dev->m_Devices);
    for (int i = 0; i < dev->m_Devices; i++) {
        if (areOK[i]) {
            tmp[0] = i;
            if (dev->m_Write(i, 0, tmp, 1) != 1) {
                areOK[i] = false;
                goto prepCreate;
            }
        }
    }
    delete [] areOK;
    return 1;
}

void RaidStart(TBlkDev * dev) {
    if(dev == NULL) return;
    if (curr != NULL) return;
    curr = new RAID(dev);
}

void RaidStop(void) {
    if (curr == NULL) return;
    delete curr;
    curr = NULL;
}

int RaidStatus(void) {
    if (curr == NULL) return RAID_STOPPED;
    else return curr->getState();
}

int RaidSize(void) {
    if (curr == NULL) return 0;
    else return (curr->wrkDev.m_Devices - 1)*(curr->wrkDev.m_Sectors - 1);
}

int RaidRead(int sector, void *data, int sectorCnt) {
    int raidState = RaidStatus();
    if (raidState == RAID_STOPPED || raidState == RAID_FAILED) return 0;
    unsigned char * realData = (unsigned char *) data;
    int i;
    for (i = 0; i < sectorCnt; i++, realData += SECTOR_SIZE) {
        int line = (sector + i) / (curr->wrkDev.m_Devices - 1);
        int parityID = curr->getParityID(line);

        int device = (sector + i) % (curr->wrkDev.m_Devices - 1);
        int readDev = (device < parityID ? device : device + 1);
        if (!curr->getData(readDev, line, realData)) return i;
    }
    return i;
}

int RaidWrite(int sector, const void *data, int sectorCnt) {
    unsigned char * realData = (unsigned char *) data;
    int prevLine = -1;
    int raidState = RaidStatus();
    if (raidState == RAID_STOPPED || raidState == RAID_FAILED) return 0;
    unsigned char parity [SECTOR_SIZE];
    unsigned char temp [SECTOR_SIZE];
    int i = 0;
    for (i = 0; i < sectorCnt; i++, realData += SECTOR_SIZE) {
        int line = (sector + i) / (curr->wrkDev.m_Devices - 1);
        if (line != prevLine) {
            if (!curr->getParityData(line, parity)) return i;
        }
        prevLine = line;
        int device = (sector + i) % (curr->wrkDev.m_Devices - 1);
        int parityID = curr->getParityID(line);
        int realDevice = (device < parityID ? device : device + 1);
        if (!curr->getData(realDevice, line, temp)) return i;
        XORArrays(parity, temp);
        XORArrays(parity, realData);
        if (curr->areOK[parityID]) {
            if (curr->wrkDev.m_Write(parityID, line + 1, parity, 1) != 1) {
                curr->areOK[parityID] = false;
            }
        }
        if (curr->areOK[realDevice]) {
            if (curr->wrkDev.m_Write(realDevice, line + 1, realData, 1) != 1) {
                curr->areOK[realDevice] = false;
            }
        }
        if (curr->getState() == RAID_FAILED) return i;
    }
    return i;
}

int RaidResync(void) {
    int state = RaidStatus();
    if (state == RAID_FAILED || state == RAID_STOPPED || state == RAID_OK) return 0;
    int repairIndex = 0;
    for (int i = 0; i < curr->wrkDev.m_Devices; i++) if (!curr->areOK[i]) {
            repairIndex = i;
            break;
        }
    for (int i = 0; i < curr->wrkDev.m_Sectors - 1; i++) {
        unsigned char data[SECTOR_SIZE];
        if (!curr->getData(repairIndex, i, data)) return 0;
        if (curr->wrkDev.m_Write(repairIndex, i + 1, data, 1) != 1) return 0;
    }
    curr->areOK[repairIndex] = true;
    return 1;
}// </editor-fold>

#ifndef __PROGTEST__
#include "tests.cpp"
#endif /* __PROGTEST__ */
